-- Create a filter CTE
WITH most_recent_assessments (personal_id, assessment_date) AS (
    SELECT cad.ref_client                     AS personal_id,
           MAX(cad.assessment_date)           AS assessment_date
      FROM clarity_client_assessment_demographics AS cad
  GROUP BY cad.ref_client
)
-- Primary query
   SELECT
          client_assessments.ref_client                                                      AS clients_id,
          client_assessments.id                                                              AS assessment_id,
          screens.name                                                                       AS assessment_name,
          client_assessment_scores.score                                                     AS coordinated_entry_assessment_score
     FROM clarity_clients AS client
LEFT JOIN clarity_client_assessment_demographics AS client_assessments ON client.id = client_assessments.ref_client AND client_assessments.deleted is NULL 
LEFT JOIN clarity_client_assessment_scores  AS client_assessment_scores ON client_assessments.id = client_assessment_scores.ref_assessment
     JOIN clarity_screens AS screens ON screens.id = client_assessments.ref_assessment
     -- The INNER JOIN should filter out all assessments but the most recent per client.
     JOIN most_recent_assessments 
       ON client_assessments.ref_client = most_recent_assessments.personal_id
      AND client_assessments.assessment_date = most_recent_assessments.assessment_date
    WHERE client.deleted IS NULL    
      AND screens.coordinated_entry IS TRUE;