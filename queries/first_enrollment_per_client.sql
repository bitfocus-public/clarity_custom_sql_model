-- Create a filter CTE
WITH first_entry_assessment (personal_id, first_enrollment_date) AS (
    SELECT cp.ref_client                     AS personal_id,
           MAX(cp.start_date)                AS first_enrollment_date
      FROM client_programs AS cp
  GROUP BY cp.ref_client
)
-- Primary query
   SELECT
          cp.ref_client                                                      AS clients_id,
          cp.id                                                              AS assessment_id,
		  cp.start_date														 AS project_start_date,
          cp.end_date														 AS project_end_date
     FROM client_programs AS cp
     JOIN client_program_demographics AS cpd 
       ON cp.id = cpd.ref_program 
      AND cpd.screen_type = 2 -- Entry Screen
     JOIN first_entry_assessment 
       ON cp.ref_client = first_entry_assessment.personal_id
      AND cp.start_date = first_entry_assessment.first_enrollment_date