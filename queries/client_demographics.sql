    SELECT 
            static_demographics.veteran_branch, -- label: Clients Branch of Military -- description: HMIS Data Element V1.11
            clients.first_name, -- label: Clients Client Full Name -- description: HMIS Data Element 3.01
            clients.last_name, -- label: Clients Client Full Name -- description: HMIS Data Element 3.01
            clients.added_date, -- label: Clients Date Created Date -- description: Date the profile was created in Clarity (or legacy system). HMIS Data Element 5.01
            clients.last_updated, -- label: Clients Date Updated -- description: Date the profile was last updated. HMIS Data Element 5.02
            clients.birth_date, -- label: Clients Date of Birth Date -- description: HMIS Data Element 3.03.1
            static_demographics.veteran_discharge, -- label: Clients Discharge Status -- description: HMIS Data Element V1.12
            clients.dob_quality, -- label: Clients DoB Data Quality -- description: HMIS Data Element 3.03.2
            static_demographics.ethnicity, -- label: Clients Ethnicity -- description: Client's hispanic or non-hispanic ethnicity. HMIS Data Element 3.05.1
            clients.first_name, -- label: Clients First Name -- description: HMIS Data Element 3.01.1
            static_demographics.gender, -- label: Clients Gender -- description: HMIS Data Element 3.06.1
            clients.last_name, -- label: Clients Last Name -- description: HMIS Data Element 3.01.3
            static_demographics.name_middle, -- label: Clients Middle Name -- description: HMIS Data Element 3.01.2
            clients.name_quality, -- label: Clients Name Data Quality -- description: HMIS Data Element 3.01.5
            clients.id, -- label: Clients Personal ID -- description: Client's ID. Includes ability to link to Clarity. HMIS Data Element 5.08
            static_demographics.race, -- label: Clients Race -- description: HMIS Data Element 3.04.1
            clients.ssn, -- label: Clients SSN -- description: HMIS Data Element 3.02.1
            clients.ssn_quality, -- label: Clients SSN Data Quality -- description: HMIS Data Element 3.02.2
            static_demographics.name_suffix, -- label: Clients Suffix -- description: HMIS Data Element 3.01.4
            static_demographics.veteran_theater_afg, -- label: Clients Theatre - Afghanistan OEF -- description: HMIS Data Element V1.7
            static_demographics.veteran_theater_iraq1, -- label: Clients Theatre - Iraq OIF -- description: HMIS Data Element V1.8
            static_demographics.veteran_theater_iraq2, -- label: Clients Theatre - Iraq OND -- description: HMIS Data Element V1.9
            static_demographics.veteran_theater_kw, -- label: Clients Theatre - Korean War -- description: HMIS Data Element V1.4
            static_demographics.veteran_theater_other, -- label: Clients Theatre - Other -- description: HMIS Data Element V1.10
            static_demographics.veteran_theater_pg, -- label: Clients Theatre - Persian Gulf ODS -- description: HMIS Data Element V1.6
            static_demographics.veteran_theater_vw, -- label: Clients Theatre - Vietnam War -- description: HMIS Data Element V1.5
            static_demographics.veteran_theater_ww2, -- label: Clients Theatre - World War II -- description: HMIS Data Element V1.3
            clients.ref_user_updated, -- label: Clients User Updating -- description: User last updating the client profile. HMIS Data Element 5.07
            static_demographics.veteran, -- label: Clients Veteran Status -- description: Is the client a US Military Veteran. HMIS Data Element 3.07.1
            static_demographics.veteran_entered, -- label: Clients Year Entered Military Service -- description: HMIS Data Element V1.1
            static_demographics.veteran_separated, -- label: Clients Year Separated from Military Service -- description: HMIS Data Element V1.2
            static_demographics.zipcode -- label: Clients ZIP Code on Profile -- description: This field is not an HMIS Data Element

      FROM clarity_clients AS clients
      JOIN clarity_client_demographics AS static_demographics ON clients.id = static_demographics.ref_client
     WHERE clients.deleted IS NULL OR clients.deleted = 0
     LIMIT 10;
