 SELECT c.id																 	AS personal_id,
        cp.id 																    AS enrollment_id,
        cp.start_date														    AS project_start_date,
        fn_c_getPicklistValueName('program_categories', p.ref_category) 		AS project_type,
        fn_c_getPicklistValueNAme('screen_types', cpd.screen_type)              AS data_collection_stage
    FROM clarity_clients AS c
    JOIN clarity_client_programs AS cp ON c.id = cp.ref_client
    JOIN clarity_programs AS p ON cp.ref_program = p.id
    JOIN client_program_demographics AS cpd ON cp.id = cpd.ref_program
   LIMIT 100;
