SELECT enrollments.id             AS enrollment_id,
      enrollments.start_date      AS project_start_date,
       enrollments.end_date         AS project_end_date
  FROM client_programs AS enrollments
 WHERE DATE("2021-01-01") <= COALESCE(enrollments.end_date, NOW())
   AND DATE("2021-03-08") >= enrollments.end_date