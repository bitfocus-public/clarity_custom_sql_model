 SELECT 
        last_screen.rhy_labor_exploitation_threats, -- label: Update/Exit Screen Afraid to quit/leave work due to threats -- description: HMIS Data Element R16.1
        last_screen.rhy_aftercare_emailsocialmedia, -- label: Update/Exit Screen Aftercare - Email/Social Media -- description: HMIS Data Element R20.2A
        last_screen.rhy_aftercare_inpersongroup, -- label: Update/Exit Screen Aftercare - In Person Group -- description: HMIS Data Element R20.2A
        last_screen.rhy_aftercare_inpersonindividual, -- label: Update/Exit Screen Aftercare - In Person Individual -- description: HMIS Data Element R20.2A
        last_screen.rhy_aftercare_telephone, -- label: Update/Exit Screen Aftercare - Telephone -- description: HMIS Data Element R20.2A
        last_screen.rhy_aftercare_yn, -- label: Update/Exit Screen Aftercare was Provided -- description: HMIS Data Element R20.2
        last_screen.income_spousal_support_is, -- label: Update/Exit Screen Alimony and other spousal support -- description: HMIS Data Element 4.02.16
        last_screen.health_ins_cobra, -- label: Update/Exit Screen COBRA -- description: HMIS Data Element 4.04.8
        last_screen.vash_case_mgt_exit_reason_other, -- label: Update/Exit Screen Case Management - Exit Reason Other -- description: HMIS Data Element V9.1B
        last_screen.vash_case_mgt_exit_reason, -- label: Update/Exit Screen Case Management - Reason Exit -- description: HMIS Data Element V9.1
        last_screen.income_childsupport_is, -- label: Update/Exit Screen Child Support -- description: HMIS Data Element 4.02.15
        last_screen.income_childsupport, -- label: Update/Exit Screen Child Support Amount -- description: HMIS Data Element 4.02.15M
        last_screen.health_chronic, -- label: Update/Exit Screen Chronic Health -- description: The client has a chronic health disabling condition. HMIS Data Element 4.07.2
        last_screen.health_chronic_longterm, -- label: Update/Exit Screen Chronic Health Longterm -- description: Chronic health condition expected to be long-continued and indefinite duration. HMIS Data Element 4.07.2A
        last_screen.prior_city, -- label: Update/Exit Screen City -- description: HMIS Data Element V5.2
        last_screen.path_status, -- label: Update/Exit Screen Client Became Enrolled in PATH -- description: HMIS Data Element P3.2
        last_screen.rhy_counseling_family, -- label: Update/Exit Screen Counseling - Family -- description: HMIS Data Element R18.1A
        last_screen.rhy_counseling_group, -- label: Update/Exit Screen Counseling - Group -- description: HMIS Data Element R18.1A
        last_screen.rhy_counseling_individual, -- label: Update/Exit Screen Counseling - Individual -- description: HMIS Data Element R18.1A
        last_screen.rhy_counseling_num_planned, -- label: Update/Exit Screen Counseling - Number Sessions Planned -- description: HMIS Data Element R18.2
        last_screen.rhy_counseling_num_sessions, -- label: Update/Exit Screen Counseling - Number Sessions Received -- description: HMIS Data Element R18.1B
        last_screen.rhy_counseling_post_exit_plan, -- label: Update/Exit Screen Counseling Plan Post Exit -- description: HMIS Data Element R18.3
        last_screen.rhy_counseling, -- label: Update/Exit Screen Counseling Received by Client -- description: HMIS Data Element R18.1
        last_screen.health_insurance, -- label: Update/Exit Screen Covered by Health Insurance -- description: HMIS Data Element 4.04.2
        last_screen.screen_type, -- label: Update/Exit Screen Data Collection Stage -- description: Identifies the Data Collection Stage. HMIS Data Element 5.03: Project Update (Status Assessment), Project Annual Assessment, Project Exit or Post Exit
        last_screen.path_engagement_date, -- label: Update/Exit Screen Date of Engagement -- description: HMIS Data Element 4.13.1
        last_screen.rhy_dental_health, -- label: Update/Exit Screen Dental Health Status -- description: HMIS Data Element R8.1
        last_screen.exit_destination, -- label: Update/Exit Screen Destination -- description: Expected living situation following project exit. HMIS Data Element 3.12
        last_screen.exit_destination_other, -- label: Update/Exit Screen Destination Other -- description: Relates to "Destination: Other" HMIS Data Element 3.12A
        last_screen.health_dev_disability, -- label: Update/Exit Screen Developmental -- description: The client has a developmental disabling condition. HMIS Data Element 4.06.2
        last_screen.health_dev_disability_independence, -- label: Update/Exit Screen Developmental Independence -- description: Retired HMIS Data Element. Developmental disability expected to substantially impair ability to live independently.
        last_screen.income_earned_is, -- label: Update/Exit Screen Earned Income -- description: HMIS Data Element 4.02.3
        last_screen.income_earned, -- label: Update/Exit Screen Earned Income Amount -- description: HMIS Data Element 4.02.3A
        last_screen.rhy_reason_not_employed, -- label: Update/Exit Screen Employed: Why not -- description: HMIS Data Element R6.2B
        last_screen.health_ins_emp, -- label: Update/Exit Screen Employer Provided -- description: HMIS Data Element 4.04.7
        -- last_screen.employed, -- label: Update/Exit Screen Employment Status -- description: HMIS Data Element R6.2
        last_screen.rhy_employment_type, -- label: Update/Exit Screen Employment Type -- description: HMIS Data Element R6.2A
        -- last_screen.engaged, -- label: Update/Exit Screen Engaged -- description: If Date of Engagement (HMIS Data Element 4.13.1) is not null on the Data Collection Screen, then ‚ÄòEngaged,' else ‚ÄòNot Engaged.‚Äô
        last_screen.rhy_exploitation, -- label: Update/Exit Screen Exchange for sex, ever received anything -- description: HMIS Data Element R15.1
        last_screen.rhy_exploitation_times, -- label: Update/Exit Screen Exchange for sex, in last three months -- description: HMIS Data Element R15.1A
        last_screen.rhy_exploitation_frequency, -- label: Update/Exit Screen Exchange for sex, number of times -- description: HMIS Data Element R15.1B
        last_screen.rhy_labor_exploitation_times, -- label: Update/Exit Screen Exploited by work, in last three months -- description: HMIS Data Element R15.1A
        last_screen.rhy_labor_exploitation_forced_tricked, -- label: Update/Exit Screen Felt forced/pressured/tricked into continuing job -- description: HMIS Data Element R16.2A
        last_screen.income_ga_is, -- label: Update/Exit Screen General Assistance -- description: HMIS Data Element 4.02.12
        last_screen.income_ga, -- label: Update/Exit Screen General Assistance Amount -- description: HMIS Data Element 4.02.12J
        last_screen.health_general, -- label: Update/Exit Screen General Health Status -- description: HMIS Data Element R7.1
        last_screen.health_hiv, -- label: Update/Exit Screen HIV/AIDS -- description: The client has HIV/AIDS. HMIS Data Element 4.08.2
        last_screen.hopwa_cobra_reason, -- label: Update/Exit Screen HOPWA: No COBRA Reason -- description: Health Insurance obtained through COBRA. HMIS Data Element 4.04.8A
        last_screen.hopwa_emp_reason, -- label: Update/Exit Screen HOPWA: No Employer Insurance Reason -- description: Employer ‚Äì Provided Health Insurance. HMIS Data Element 4.04.7A
        last_screen.indian_health_reason, -- label: Update/Exit Screen HOPWA: No Indian Health Services Reason -- description: Indian Health Services Program. HMIS Data Element 4.04.11A
        last_screen.hopwa_medicaid_reason, -- label: Update/Exit Screen HOPWA: No Medicaid Reason -- description: HMIS Data Element 4.04.3A
        last_screen.hopwa_medicare_reason, -- label: Update/Exit Screen HOPWA: No Medicare Reason -- description: HMIS Data Element 4.04.4A
        last_screen.hopwa_ppay_reason, -- label: Update/Exit Screen HOPWA: No Private Pay Health Insurance Reason -- description: HMIS Data Element 4.04.9A
        last_screen.hopwa_schip_reason, -- label: Update/Exit Screen HOPWA: No SCHIP Reason -- description: State Children‚Äôs Health Insurance Program. HMIS Data Element 4.04.5A
        last_screen.hopwa_state_reason, -- label: Update/Exit Screen HOPWA: No State Insurance Reason -- description: State Health Insurance for Adults. HMIS Data Element 4.04.10A
        last_screen.hopwa_va_medical_reason, -- label: Update/Exit Screen HOPWA: No VA Medical Reason -- description: Veteran‚Äôs Administration (VA) Medical Services. HMIS Data Element 4.04.6A
        last_screen.housing_ass_exit, -- label: Update/Exit Screen Housing Assessment at Exit -- description: Homelessness Prevention question, did client maintian or change housing situation while in program. HMIS Data Element W5.1
        last_screen.housing_ass_exit_2, -- label: Update/Exit Screen Housing Assessment at Exit New Subsidy -- description: If moved to new housing unit for Housing Assessment at Exit. HMIS Data Element W5.1B
        last_screen.housing_ass_exit_1, -- label: Update/Exit Screen Housing Assessment at Exit Subsidy Maintained -- description: If able to maintain the housing they had at project entry for housing assessment at exit. HMIS Data Element W5.1A
        last_screen.housing_status, -- label: Update/Exit Screen Housing Status Text -- description: Retired HMIS Data Element
        last_screen.hopwa_hiv_adap_reason, -- label: Update/Exit Screen If not Receiving AIDS Drug Assistance Program (ADAP) Reason, why? -- description: If No for "Receiving AIDS Drug Assistance Program (ADAP)" Reason. HMIS Data Element W3.3B
        last_screen.hopwa_hiv_services_reason, -- label: Update/Exit Screen If not Receiving Public HIV/AIDS Medical Assistance Reason, why? -- description: If No for "Receiving Public HIV/AIDS Medical Assistance" Reason. HMIS Data Element W3.2A
        last_screen.income_other, -- label: Update/Exit Screen Income Amount: Other -- description: HMIS Data Element 4.02.17O
        last_screen.income_private_pension, -- label: Update/Exit Screen Income Private Pension -- description: HMIS Data Element 4.02.14L
        last_screen.income_other_source, -- label: Update/Exit Screen Income Source: Other -- description: HMIS Data Element 4.02.17P
        last_screen.income_spousal_support, -- label: Update/Exit Screen Income Spousal Support -- description: HMIS Data Element 4.02.16N
        last_screen.income_tanf, -- label: Update/Exit Screen Income TANF -- description: HMIS Data Element 4.02.11I
        last_screen.income_vet_disability, -- label: Update/Exit Screen Income Vet Disability -- description: HMIS Data Element 4.02.7E
        last_screen.income_vet_pension, -- label: Update/Exit Screen Income Vet Pension -- description: HMIS Data Element 4.02.8
        last_screen.income_workers_comp, -- label: Update/Exit Screen Income Workers Comp -- description: HMIS Data Element 4.02.10H
        last_screen.income_other_is, -- label: Update/Exit Screen Income: Other -- description: HMIS Data Element 4.02.17
        last_screen.c_indian_health_service_program, -- label: Update/Exit Screen Indian Health Services Program -- description: HMIS Data Element 4.04.11
        last_screen.program_date, -- label: Update/Exit Screen Information Date -- description: Date set by the user for Status/Annual Assessment or Project Exit. HMIS Data Element 5.04
        last_screen.rhy_education_level, -- label: Update/Exit Screen Last Grade Completed -- description: HMIS Data Element R4.1
        last_screen.last_updated, -- label: Update/Exit Screen Last Updated Date -- description: Date the Status/Annual Assessment or Project Exit was last updated. HMIS Data Element 5.02
        last_screen.rhy_exploitation_ask, -- label: Update/Exit Screen Made/persuaded to have sex in exchange for something -- description: HMIS Data Element R15.1C
        last_screen.rhy_exploitation_ask_times, -- label: Update/Exit Screen Made/persuaded to have sex, in last three months -- description: HMIS Data Element R15.1D
        last_screen.benefits_medicaid, -- label: Update/Exit Screen Medicaid -- description: On Medicaid Insurance. HMIS Data Element 4.04.3
        last_screen.benefits_medicare, -- label: Update/Exit Screen Medicare -- description: On Medicare Insurance. HMIS Data Element 4.04.4
        last_screen.health_mental, -- label: Update/Exit Screen Mental Health -- description: The client has a mental health disabling condition. HMIS Data Element 4.09.1
        last_screen.health_mental_longterm, -- label: Update/Exit Screen Mental Health Longterm -- description: Mental health disabling condition expected to be long-continued and indefinite duration. HMIS Data Element 4.09.2
        last_screen.rhy_mental_health, -- label: Update/Exit Screen Mental Health Status -- description: HMIS Data Element R9.1
        last_screen.benefits_noncash, -- label: Update/Exit Screen Non-Cash Benefit from Any Source -- description: Is the client receiving any non-cash benefits. HMIS Data Element 4.03.2
        last_screen.benefits_other, -- label: Update/Exit Screen Non-Cash Benefits: Other -- description: HMIS Data Element 4.03.8
        last_screen.benefits_other_source, -- label: Update/Exit Screen Non-Cash Benefits: Other Source -- description: HMIS Data Element 4.03.8A
        last_screen.other_health_insurance, -- label: Update/Exit Screen Other Health Insurance -- description: HMIS Data Element 4.04.12
        last_screen.other_health_insurance_specify, -- label: Update/Exit Screen Other Health Insurance Source -- description: HMIS Data Element 4.04.12
        last_screen.benefits_tanf_other, -- label: Update/Exit Screen Other TANF-funded services -- description: Receiving Other TANF-funded services benefits. HMIS Data Element 4.03.7
        last_screen.income_private_pension_is, -- label: Update/Exit Screen Pension or retirement income from a former job -- description: HMIS Data Element 4.02.14
        last_screen.health_phys_disability, -- label: Update/Exit Screen Physical -- description: The client has a physical disabling condition. HMIS Data Element 4.05.2
        last_screen.health_phys_disability_longterm, -- label: Update/Exit Screen Physical Longterm -- description: Physical disabling condition expected to be long-continued and indefinite duration. HMIS Data Element 4.05.2A
        last_screen.rhy_adult_connect, -- label: Update/Exit Screen Positive Connections - Adult -- description: Client has permanent positive adult connections outside of project. HMIS Data Element R19.3
        last_screen.rhy_community_connect, -- label: Update/Exit Screen Positive Connections - Community -- description: Client has permanent positive community connections outside of project. HMIS Data Element R19.5
        last_screen.rhy_peer_connect, -- label: Update/Exit Screen Positive Connections - Peer -- description: Client has permanent positive peer connections outside of project. HMIS Data Element R19.4
        last_screen.income_private_disability, -- label: Update/Exit Screen Private Disability Insurance -- description: HMIS Data Element 4.02.9G
        last_screen.income_private_disability_is, -- label: Update/Exit Screen Private Disability Insurance -- description: HMIS Data Element 4.02.9
        last_screen.health_ins_ppay, -- label: Update/Exit Screen Private Pay -- description: HMIS Data Element 4.04.9
        last_screen.rhy_completion_status, -- label: Update/Exit Screen Project Completion Status -- description: HMIS Data Element R17.1
        last_screen.rhy_completion_involuntary_reason, -- label: Update/Exit Screen Project Completion Status: Reason for Involuntary Discharge -- description: HMIS Data Element R17.1A
        last_screen.rhy_labor_exploitation_payment, -- label: Update/Exit Screen Promised work, work or payment different -- description: HMIS Data Element R16.2
        last_screen.path_not_enrolled_reason, -- label: Update/Exit Screen Reason not Enrolled -- description: HMIS Data Element P3.2A
        last_screen.hopwa_hiv_adap, -- label: Update/Exit Screen Receiving AIDS Drug Assistance Program (ADAP) -- description: HMIS Data Element W3.3
        last_screen.hopwa_hiv_services, -- label: Update/Exit Screen Receiving Public HIV/AIDS Medical Assistance -- description: HMIS Data Element W3.2
        last_screen.prior_residence, -- label: Update/Exit Screen Residence Prior to Project Entry -- description: HMIS Data Element 3.917AB.1
        last_screen.income_ss_retirement, -- label: Update/Exit Screen Retirement Income from Social Security -- description: HMIS Data Element 4.02.13K
        last_screen.benefits_schip, -- label: Update/Exit Screen SCHIP -- description: On State Children Health Insurance Program. HMIS Data Element 4.04.5
        last_screen.soar_connected, -- label: Update/Exit Screen SOAR Connected -- description: Client is connected to SSI/SSDI Outreach, Access and Recovery technical assistance program. HMIS Data Element P4.1
        last_screen.income_ssdi, -- label: Update/Exit Screen SSDI Amount -- description: HMIS Data Element 4.02.6D
        last_screen.income_ssi, -- label: Update/Exit Screen SSI Amount -- description: HMIS Data Element 4.02.5C
        last_screen.rhy_dest_safe_caseworker, -- label: Update/Exit Screen Safe Exit - Caseworker -- description: Exit destination safe ‚Äì as determined by the project/caseworker. HMIS Data Element R19.2
        last_screen.rhy_dest_safe_client, -- label: Update/Exit Screen Safe Exit - Client -- description: Exit destination safe ‚Äì as determined by the client. HMIS Data Element R19.1
        last_screen.rhy_school_status, -- label: Update/Exit Screen School Status -- description: HMIS Data Element R5.1
        last_screen.benefits_section8, -- label: Update/Exit Screen Section 8 -- description: Retired HMIS Data Element
        last_screen.income_ssdi_is, -- label: Update/Exit Screen Social Security Disability Insurance (SSDI) -- description: HMIS Data Element 4.02.6
        last_screen.income_ss_retirement_is, -- label: Update/Exit Screen Social Security Retirement Income -- description: HMIS Data Element 4.02.13
        last_screen.benefits_wic, -- label: Update/Exit Screen Special Supplemental Nutrition Program for Women, Infants, and Children (WIC) -- description: Receiving Women, Infants and Children benefits. HMIS Data Element 4.03.4
        last_screen.prior_state, -- label: Update/Exit Screen State -- description: HMIS Data Element V5.3
        last_screen.health_ins_state, -- label: Update/Exit Screen State Insurance for Adults -- description: HMIS Data Element 4.04.10
        last_screen.path_status_determination, -- label: Update/Exit Screen Status Determination Date -- description: HMIS Data Element P3.1
        last_screen.prior_street_address, -- label: Update/Exit Screen Street Address -- description: HMIS Data Element V5.1
        last_screen.health_substance_abuse, -- label: Update/Exit Screen Substance Abuse -- description: The client has a substance abuse disabling condition. HMIS Data Element 4.10.2
        last_screen.health_substance_abuse_longterm, -- label: Update/Exit Screen Substance Abuse Longterm -- description: Substance abuse disabling condition expected to be long-continued and indefinite duration. HMIS Data Element 4.10.2A
        -- last_screen.benefits_snap, -- label: Update/Exit Screen Supplemental Nutrition Assistance Program (SNAP) (Previously known as Food Stamps) -- description: Supplemental Nutrition Assistance Program (SNAP; Previously known as Food Stamps). HMIS Data Element 4.03.3
        last_screen.income_ssi_is, -- label: Update/Exit Screen Supplemental Security Income (SSI) -- description: HMIS Data Element 4.02.5
        last_screen.tcell_count_available, -- label: Update/Exit Screen T-cell (CD4) Count Available -- description: HMIS Data Element W4.2
        last_screen.tcell_count_number, -- label: Update/Exit Screen T-cell Count -- description: (integer between 0 - 1500) HMIS Data Element W4.2A
        last_screen.tcell_data_obtained, -- label: Update/Exit Screen T-cell Count obtained -- description: How Was the information Obtained (T-cell Count) HMIS Data Element W4.2B
        last_screen.income_tanf_is, -- label: Update/Exit Screen TANF -- description: HMIS Data Element 4.02.11I
        last_screen.benefits_tanf_childcare, -- label: Update/Exit Screen TANF Child Care services -- description: Receiving TANF Childcare benefits. HMIS Data Element 4.03.5
        last_screen.benefits_tanf_transportation, -- label: Update/Exit Screen TANF transportation services -- description: Receiving TANF transportation services benefits. HMIS Data Element 4.03.6
        last_screen.benefits_temp_rent, -- label: Update/Exit Screen Temporary Rental Assistance -- description: Retired HMIS Data Element
        last_screen.chronic_2, -- label: Update/Exit Screen Times Homeless in the Past Three Years -- description: Times homeless, including this time, in past three years. HMIS Data Element 3.917AB.4
        last_screen.income_individual, -- label: Update/Exit Screen Total Cash Income -- description: Sum of cash income from all sources for individual. HMIS Data Element 4.02.18
        last_screen.chronic_3, -- label: Update/Exit Screen Total Months Homeless in Past Three Years -- description: Total months homeless in past three years. HMIS Data Element 3.917AB.5
        last_screen.income_unemployment, -- label: Update/Exit Screen Unemployment Amount -- description: HMIS Data Element 4.02.4B
        last_screen.income_unemployment_is, -- label: Update/Exit Screen Unemployment Income -- description: HMIS Data Element 4.02.4
        last_screen.ref_user, -- label: Update/Exit Screen User Creating -- description: User that created Project Update, Annual Assessment, Exit or Post Exit. HMIS Data Element 5.07
        last_screen.ref_user_updated, -- label: Update/Exit Screen User Updating -- description: User that updated Project Update, Annual Assessment, Exit or Post Exit. HMIS Data Element 5.07
        last_screen.benefits_va_medical, -- label: Update/Exit Screen VA Medical Insurance -- description: On VA Medical Insurance. HMIS Data Element 4.04.6
        last_screen.income_vet_disability_is, -- label: Update/Exit Screen Veteran Disability -- description: HMIS Data Element 4.02.7
        last_screen.income_vet_pension_is, -- label: Update/Exit Screen Veteran Pension -- description: HMIS Data Element 4.02.8F
        last_screen.viral_load_available, -- label: Update/Exit Screen Viral Load Information Available -- description: HMIS Data Element W4.3
        last_screen.viral_load_number, -- label: Update/Exit Screen Viral Load Number -- description: Count (integer between 0 - 999999) HMIS Data Element W4.3C
        last_screen.viral_load_obtained, -- label: Update/Exit Screen Viral Load Obtained -- description: How Was the information Obtained (Viral Load). HMIS Data Element W4.3D
        last_screen.income_workers_comp_is, -- label: Update/Exit Screen Workers Comp -- description: HMIS Data Element 4.02.10
        last_screen.zipcode -- label: Update/Exit Screen ZIP Code -- description: HMIS Data Element V5.4

 FROM client_programs AS enrollments
 JOIN client_program_demographics AS last_screen ON enrollments.id = last_screen.ref_program AND last_screen.deleted IS NULL
WHERE enrollments.deleted IS NULL;