   SELECT
          client_assessments.ref_client                                                      AS clients_id,
          client_assessments.id                                                              AS assessment_id,
          screens.name                                                                       AS assessment_name,
          client_assessment_scores.score                                                     AS assessment_score,
          client_assessments.ref_agency                                                      AS agency_id, 
          (SELECT a.name FROM agencies AS a WHERE a.id = client_assessments.ref_agency)      AS agency_name,
          CASE screens.coordinated_entry
               WHEN TRUE THEN "Yes"
               WHEN FALSE THEN "No" 
               ELSE screens.coordinated_entry
          END AS coordinated_entry_assessment
     FROM clarity_clients AS client
LEFT JOIN clarity_client_assessment_demographics AS client_assessments ON client.id = client_assessments.ref_client AND client_assessments.deleted is NULL 
LEFT JOIN clarity_client_assessment_scores  AS client_assessment_scores ON client_assessments.id = client_assessment_scores.ref_assessment
     JOIN clarity_screens AS screens ON screens.id = client_assessments.ref_assessment
    WHERE client.deleted IS NULL    
    LIMIT 10;