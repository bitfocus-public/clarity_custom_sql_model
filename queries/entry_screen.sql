SELECT
        enrollments.added_date, -- label: Enrollments Date Created Date -- description: Date the Project Enrollment was created. HMIS Data Element 5.01
        enrollments.last_updated, -- label: Enrollments Date Updated Date -- description: Date the Project Enrollment was last updated. HMIS Data Element 5.02
        enrollments.id, -- label: Enrollments Enrollment ID -- description: HMIS Data Element 5.06
        enrollments.ref_household, -- label: Enrollments Household ID -- description: Household ID unique to Project Enrollment. HMIS Data Element 5.09
        enrollments.end_date, -- label: Enrollments Project Exit Date -- description: Project Exit Date. HMIS Data Element 3.11.1
        enrollments.start_date, -- label: Enrollments Project Start Date -- description: Project Start. HMIS Data Element 3.10.1
        enrollments.ref_user, -- label: Enrollments User Creating -- description: User that created the enrollment record. HMIS Data Element 5.07
        enrollments.ref_user_updated, -- label: Enrollments User Updating -- description: User who created &/or updated the enrollment record. HMIS Data Element 5.07
        entry_screen.prior_address_quality, -- label: Entry Screen Address Data Quality -- description: HMIS Data Element V5.5
        entry_screen.rhy_labor_exploitation_threats, -- label: Entry Screen Afraid to quit/leave work due to threats -- description: HMIS Data Element R16.1
        entry_screen.rhy_crit_substance_family, -- label: Entry Screen Alcohol or Other Drug Abuse - Family -- description: HMIS Data Element R13.21
        entry_screen.income_spousal_support_is, -- label: Entry Screen Alimony and other spousal support -- description: HMIS Data Element 4.02.16
        entry_screen.ssvf_targeting_field_4, -- label: Entry Screen Annual Household Gross Income Amount (Yes / No) -- description: HMIS Data Element V7.4
        entry_screen.ssvf_targeting_field_16, -- label: Entry Screen Any Veteran in Household Served in Iraq or Afghanistan (Yes / No) -- description: HMIS Data Element V7.16
        entry_screen.chronic_7, -- label: Entry Screen Approximate Date Homelessness Started Date -- description: HMIS Data Element 3.917AB.3
        entry_screen.ssvf_targeting_field_13, -- label: Entry Screen At least one dependent child under age 6 (Yes / No) -- description: HMIS Data Element V7.13
        entry_screen.rhy_no_svc_reason, -- label: Entry Screen BCP Status: Reason for not providing services -- description: Reason why services are not funded by BCP grant. HMIS Data Element R2.2
        entry_screen.health_ins_cobra, -- label: Entry Screen COBRA -- description: HMIS Data Element 4.04.8
        entry_screen.income_childsupport_is, -- label: Entry Screen Child Support -- description: HMIS Data Element 4.02.15
        entry_screen.income_childsupport, -- label: Entry Screen Child Support Amount -- description: HMIS Data Element 4.02.15M
        entry_screen.chronic_2, -- label: Entry Screen Chronic 2 Raw -- description: Times homeless, including this time, in past three years. HMIS Data Element 3.917AB.4
        entry_screen.chronic_3, -- label: Entry Screen Chronic 3 Raw -- description: Total months homeless in past three years. HMIS Data Element 3.917AB.5
        entry_screen.chronic_6, -- label: Entry Screen Chronic 6 Raw -- description: On the night before entering the most recent type of residence, record whether the client stayed on the streets, ES or SH HMIS Data Element 3.917B.2C
        entry_screen.health_chronic, -- label: Entry Screen Chronic Health -- description: The client has a chronic health disabling condition. HMIS Data Element 4.07.2
        entry_screen.health_chronic_documented, -- label: Entry Screen Chronic Health Documented -- description: Retired HMIS Data Element. Chronic health condition is documented.
        entry_screen.health_chronic_longterm, -- label: Entry Screen Chronic Health Longterm -- description: Chronic health condition expected to be long-continued and indefinite duration. HMIS Data Element 4.07.2A
        entry_screen.prior_city, -- label: Entry Screen City -- description: HMIS Data Element V5.2
        -- entry_screen.path_enrollment_status, -- label: Entry Screen Client Became Enrolled in PATH -- description: HMIS Data Element P3.2
        entry_screen.client_location, -- label: Entry Screen CoC Code of Client at Project Start -- description: HMIS Data Element 3.16.2
        entry_screen.health_insurance, -- label: Entry Screen Covered by Health Insurance -- description: HMIS Data Element 4.04.2
        entry_screen.ssvf_targeting_field_11, -- label: Entry Screen Criminal Record for Arson, Drug Dealing or Manufacture, or Felony Offense Against Persons or Property (Yes / No) -- description: HMIS Data Element V7.11
        entry_screen.ssvf_targeting_field_3, -- label: Entry Screen Current Household Income is $0 (Yes / No) -- description: HMIS Data Element V7.3
        entry_screen.ssvf_targeting_field_2, -- label: Entry Screen Current Housing Loss Expected Within: (Yes / No) -- description: HMIS Data Element V7.2
        entry_screen.health_dv_fleeing, -- label: Entry Screen Currently Fleeing Domestic Violence -- description: HMIS Data Element 4.11.2B
        entry_screen.ssvf_targeting_field_8, -- label: Entry Screen Currently at Risk of Losing a Tenant-Based Housing Subsidy or Housing in a Subsidized Building or Unit (Yes / No) -- description: HMIS Data Element V7.8
        entry_screen.screen_type, -- label: Entry Screen Data Collection Stage -- description: Identifies the Data Collection Stage. HMIS Data Element 5.03: Project Update (Status Assessment), Project Annual Assessment, Project Exit or Post Exit
        -- entry_screen.bcp_status_determination, -- label: Entry Screen Date of BCP Status Determination Date -- description: Date of BCP Status Determination. HMIS Data Element R2.1
        -- entry_screen.bcp_status_determination_week, -- label: Entry Screen Date of BCP Status Determination Week -- description: Date of BCP Status Determination. HMIS Data Element R2.1
        entry_screen.path_engagement_date, -- label: Entry Screen Date of Engagement -- description: HMIS Data Element 4.13.1
        entry_screen.rhy_dental_health, -- label: Entry Screen Dental Health Status -- description: HMIS Data Element R8.1
        entry_screen.health_dev_disability, -- label: Entry Screen Developmental -- description: The client has a developmental disabling condition. HMIS Data Element 4.06.2
        entry_screen.health_dev_disability_documented, -- label: Entry Screen Developmental Documented -- description: Retired HMIS Data Element. Developmental disability is documented.
        entry_screen.health_dev_disability_independence, -- label: Entry Screen Developmental Independence -- description: Retired HMIS Data Element. Developmental disability expected to substantially impair ability to live independently.
        entry_screen.disabled, -- label: Entry Screen Disabling Condition -- description: Does the client have a disabling condition that is expected to be of long-continued and indefinite duration and substantially impairs ability to live independently. HMIS Data Element 3.08.1
        entry_screen.health_dv, -- label: Entry Screen Domestic Violence -- description: HMIS Data Element 4.11.2
        entry_screen.health_dv_occurred, -- label: Entry Screen Domestic Violence Occurred -- description: HMIS Data Element 4.11.2A
        entry_screen.income_earned_is, -- label: Entry Screen Earned Income -- description: HMIS Data Element 4.02.3
        entry_screen.income_earned, -- label: Entry Screen Earned Income Amount -- description: HMIS Data Element 4.02.3A
        entry_screen.rhy_reason_not_employed, -- label: Entry Screen Employed: Why not -- description: HMIS Data Element R6.2B
        entry_screen.health_ins_emp, -- label: Entry Screen Employer Provided -- description: HMIS Data Element 4.04.7
        -- entry_screen.employed, -- label: Entry Screen Employment Status -- description: HMIS Data Element R6.2
        entry_screen.rhy_employment_type, -- label: Entry Screen Employment Type -- description: HMIS Data Element R6.2A
        entry_screen.rhy_exploitation, -- label: Entry Screen Exchange for sex, ever received anything -- description: HMIS Data Element R15.1
        entry_screen.rhy_exploitation_times, -- label: Entry Screen Exchange for sex, in last three months -- description: HMIS Data Element R15.1A
        entry_screen.rhy_exploitation_frequency, -- label: Entry Screen Exchange for sex, number of times -- description: HMIS Data Element R15.1B
        entry_screen.rhy_labor_exploitation_times, -- label: Entry Screen Exploited by work, in last three months -- description: HMIS Data Element R15.1A
        entry_screen.rhy_fysb_youth, -- label: Entry Screen FYSB Youth -- description: Youth Eligible for RHY Services. HMIS Data Element R2.2
        entry_screen.rhy_labor_exploitation_forced_tricked, -- label: Entry Screen Felt forced/pressured/tricked into continuing job -- description: HMIS Data Element R16.2A
        entry_screen.ssvf_targeting_field_17, -- label: Entry Screen Female Veteran (Yes / No) -- description: HMIS Data Element V7.17
        entry_screen.previous_foster_care, -- label: Entry Screen Foster Care: Former Ward -- description: HMIS Data Element R11.1
        entry_screen.rhy_foster_length_years, -- label: Entry Screen Foster Care: Number of Years -- description: HMIS Data Element R11.1A
        entry_screen.income_ga_is, -- label: Entry Screen General Assistance -- description: HMIS Data Element 4.02.12
        entry_screen.income_ga, -- label: Entry Screen General Assistance Amount -- description: HMIS Data Element 4.02.12J
        entry_screen.health_general, -- label: Entry Screen General Health Status -- description: HMIS Data Element R7.1
        entry_screen.ssvf_targeting_field_21, -- label: Entry Screen Grantee Targeting Threshold Score (integer) -- description: HMIS Data Element V7.21
        entry_screen.health_hiv, -- label: Entry Screen HIV/AIDS -- description: The client has HIV/AIDS. HMIS Data Element 4.08.2
        entry_screen.health_hiv_documented, -- label: Entry Screen HIV/AIDS Documented -- description: Retired HMIS Data Element. HIV/AIDS disability is documented.
        entry_screen.health_hiv_independence, -- label: Entry Screen HIV/AIDS Independence -- description: Retired HMIS Data Element. HIV/AIDS disability expected to substantially impair ability to live independently.
        entry_screen.health_hiv_services, -- label: Entry Screen HIV/AIDS Services -- description: Retired HMIS Data Element. Client is receiving services for HIV/AIDS.
        entry_screen.hopwa_cobra_reason, -- label: Entry Screen HOPWA: No COBRA Reason -- description: Health Insurance obtained through COBRA. HMIS Data Element 4.04.8A
        entry_screen.hopwa_emp_reason, -- label: Entry Screen HOPWA: No Employer Insurance Reason -- description: Employer ‚Äì Provided Health Insurance. HMIS Data Element 4.04.7A
        entry_screen.indian_health_reason, -- label: Entry Screen HOPWA: No Indian Health Services Reason -- description: Indian Health Services Program. HMIS Data Element 4.04.11A
        entry_screen.hopwa_medicaid_reason, -- label: Entry Screen HOPWA: No Medicaid Reason -- description: HMIS Data Element 4.04.3A
        entry_screen.hopwa_medicare_reason, -- label: Entry Screen HOPWA: No Medicare Reason -- description: HMIS Data Element 4.04.4A
        entry_screen.hopwa_ppay_reason, -- label: Entry Screen HOPWA: No Private Pay Health Insurance Reason -- description: HMIS Data Element 4.04.9A
        entry_screen.hopwa_schip_reason, -- label: Entry Screen HOPWA: No SCHIP Reason -- description: State Children‚Äôs Health Insurance Program. HMIS Data Element 4.04.5A
        entry_screen.hopwa_state_reason, -- label: Entry Screen HOPWA: No State Insurance Reason -- description: State Health Insurance for Adults. HMIS Data Element 4.04.10A
        entry_screen.hopwa_va_medical_reason, -- label: Entry Screen HOPWA: No VA Medical Reason -- description: Veteran‚Äôs Administration (VA) Medical Services. HMIS Data Element 4.04.6A
        entry_screen.ssvf_targeting_field_20, -- label: Entry Screen HP Applicant Total Points (integer) -- description: HMIS Data Element V7.20
        entry_screen.ssvf_targeting_field_10, -- label: Entry Screen Head of Household with Disabling Condition (Physical Health, Mental Health, Substance Use) That Directly Affects Ability to Secure/Maintain Housing (Yes / No) -- description: HMIS Data Element V7.10
        entry_screen.ssvf_targeting_field_9, -- label: Entry Screen History of Literal Homelessness (Street/Shelter/Transitional Housing) (Yes / No) -- description: HMIS Data Element V7.9
        entry_screen.ssvf_targeting_field_15, -- label: Entry Screen Household Size of 5 or More Requiring At Least 3 Bedrooms (Due to Age/Gender Mix) (Yes / No) -- description: HMIS Data Element V7.15
        entry_screen.housing_ass_exit, -- label: Entry Screen Housing Assessment at Exit -- description: Homelessness Prevention question, did client maintian or change housing situation while in program. HMIS Data Element W5.1
        entry_screen.housing_ass_exit_2, -- label: Entry Screen Housing Assessment at Exit New Subsidy -- description: If moved to new housing unit for Housing Assessment at Exit. HMIS Data Element W5.1B
        entry_screen.housing_ass_exit_1, -- label: Entry Screen Housing Assessment at Exit Subsidy Maintained -- description: If able to maintain the housing they had at project entry for housing assessment at exit. HMIS Data Element W5.1A
        entry_screen.move_in_date, -- label: Entry Screen Housing Move-in Date -- description: HMIS Data Element 3.20
        entry_screen.hopwa_hiv_adap_reason, -- label: Entry Screen If not Receiving AIDS Drug Assistance Program (ADAP) Reason, why? -- description: If No for 'Receiving AIDS Drug Assistance Program (ADAP)' Reason. HMIS Data Element W3.3B
        entry_screen.hopwa_hiv_services_reason, -- label: Entry Screen If not Receiving Public HIV/AIDS Medical Assistance Reason, why? -- description: If No for 'Receiving Public HIV/AIDS Medical Assistance' Reason. HMIS Data Element W3.2A
        entry_screen.rhy_crit_incarcerated_parent, -- label: Entry Screen Incarcerated Parent of Youth - Family -- description: HMIS Data Element R13.24
        entry_screen.income_other, -- label: Entry Screen Income Amount: Other -- description: HMIS Data Element 4.02.17O
        entry_screen.income_private_pension, -- label: Entry Screen Income Private Pension -- description: HMIS Data Element 4.02.14L
        entry_screen.income_other_source, -- label: Entry Screen Income Source: Other -- description: HMIS Data Element 4.02.17P
        entry_screen.income_spousal_support, -- label: Entry Screen Income Spousal Support -- description: HMIS Data Element 4.02.16N
        entry_screen.income_tanf, -- label: Entry Screen Income TANF -- description: HMIS Data Element 4.02.11I
        entry_screen.income_vet_disability, -- label: Entry Screen Income Vet Disability -- description: HMIS Data Element 4.02.7E
        entry_screen.income_vet_pension, -- label: Entry Screen Income Vet Pension -- description: HMIS Data Element 4.02.8
        entry_screen.income_workers_comp, -- label: Entry Screen Income Workers Comp -- description: HMIS Data Element 4.02.10H
        entry_screen.income_other_is, -- label: Entry Screen Income: Other -- description: HMIS Data Element 4.02.17
        entry_screen.c_indian_health_service_program, -- label: Entry Screen Indian Health Services Program -- description: HMIS Data Element 4.04.11
        entry_screen.program_date, -- label: Entry Screen Information Date -- description: Date set by the user for Status/Annual Assessment or Project Exit. HMIS Data Element 5.04
        entry_screen.institutional_90_days, -- label: Entry Screen Institutional 90 Days Raw -- description: For institutional situations, was stay less than 90 days HMIS Data Element 3.917B.2A
        entry_screen.rhy_crit_income_family, -- label: Entry Screen Insufficient Income - Family -- description: HMIS Data Element R13.22
        entry_screen.runaway_youth, -- label: Entry Screen Is Runaway Youth -- description: HMIS Data Element R2.2B
        entry_screen.rhy_former_justice, -- label: Entry Screen Juvenile Justice: Former Ward -- description: HMIS Data Element R12.1
        entry_screen.rhy_justice_length_years, -- label: Entry Screen Juvenile Justice: Number of Years -- description: HMIS Data Element R12.1A
        entry_screen.rhy_education_level, -- label: Entry Screen Last Grade Completed -- description: HMIS Data Element R4.1
        entry_screen.last_updated, -- label: Entry Screen Last Updated Date -- description: Date the Status/Annual Assessment or Project Exit was last updated. HMIS Data Element 5.02
        entry_screen.th_ph_less_than_7_nights, -- label: Entry Screen Length of Stay Less Than 7 Days -- description: For transitional and permanent housing situations, was stay less than 7 days HMIS Data Element 3.917B.2B
        entry_screen.institutional_90_days, -- label: Entry Screen Length of Stay Less Than 90 Days -- description: For institutional situations, was stay less than 90 days HMIS Data Element 3.917B.2A
        entry_screen.prior_duration, -- label: Entry Screen Length of Stay in Prior Living Situation -- description: HMIS Data Element 3.917AB.2
        entry_screen.rhy_exploitation_ask, -- label: Entry Screen Made/persuaded to have sex in exchange for something -- description: HMIS Data Element R15.1C
        entry_screen.rhy_exploitation_ask_times, -- label: Entry Screen Made/persuaded to have sex, in last three months -- description: HMIS Data Element R15.1D
        entry_screen.ssvf_targeting_field_6, -- label: Entry Screen Major Change in Household Composition (e.g., Death of Family Member, Separation/Divorce from Adult Partner, Birth of New Child) in the Past 12 Months (Yes / No) -- description: HMIS Data Element V7.6
        entry_screen.benefits_medicaid, -- label: Entry Screen Medicaid -- description: On Medicaid Insurance. HMIS Data Element 4.04.3
        entry_screen.benefits_medicare, -- label: Entry Screen Medicare -- description: On Medicare Insurance. HMIS Data Element 4.04.4
        entry_screen.health_mental, -- label: Entry Screen Mental Health -- description: The client has a mental health disabling condition. HMIS Data Element 4.09.1
        entry_screen.health_mental_confirmed, -- label: Entry Screen Mental Health Confirmed (PATH) -- description: Retired HMIS Data Element. PATH Only: How was mental health disabling condition confirmed.
        entry_screen.health_mental_documented, -- label: Entry Screen Mental Health Documented -- description: Retired HMIS Data Element. Mental health disabling condition is documented.
        entry_screen.rhy_crit_mental_family, -- label: Entry Screen Mental Health Issues - Family -- description: HMIS Data Element R13.11
        entry_screen.health_mental_longterm, -- label: Entry Screen Mental Health Longterm -- description: Mental health disabling condition expected to be long-continued and indefinite duration. HMIS Data Element 4.09.2
        entry_screen.health_mental_smi, -- label: Entry Screen Mental Health SMI (PATH) -- description: Retired HMIS Data Element. PATH Only: Serious mental illness and how confirmed.
        entry_screen.rhy_mental_health, -- label: Entry Screen Mental Health Status -- description: HMIS Data Element R9.1
        entry_screen.benefits_noncash, -- label: Entry Screen Non-Cash Benefit from Any Source -- description: Is the client receiving any non-cash benefits. HMIS Data Element 4.03.2
        entry_screen.benefits_other, -- label: Entry Screen Non-Cash Benefits: Other -- description: HMIS Data Element 4.03.8
        entry_screen.benefits_other_source, -- label: Entry Screen Non-Cash Benefits: Other Source -- description: HMIS Data Element 4.03.8A
        entry_screen.chronic_6, -- label: Entry Screen On the night before - stayed on the streets, ES or Safe Haven -- description: On the night before entering the most recent type of residence, record whether the client stayed on the streets, ES or SH HMIS Data Element 3.917B.2C
        entry_screen.other_health_insurance, -- label: Entry Screen Other Health Insurance -- description: HMIS Data Element 4.04.12
        entry_screen.other_health_insurance_specify, -- label: Entry Screen Other Health Insurance Source -- description: HMIS Data Element 4.04.12
        entry_screen.benefits_tanf_other, -- label: Entry Screen Other TANF-funded services -- description: Receiving Other TANF-funded services benefits. HMIS Data Element 4.03.7
        entry_screen.income_private_pension_is, -- label: Entry Screen Pension or retirement income from a former job -- description: HMIS Data Element 4.02.14
        entry_screen.ami_percent, -- label: Entry Screen Percent of AMI -- description: HMIS Data Element V4.1 Percent of Area Median Income
        entry_screen.health_phys_disability, -- label: Entry Screen Physical -- description: The client has a physical disabling condition. HMIS Data Element 4.05.2
        entry_screen.rhy_crit_disability_physical_family, -- label: Entry Screen Physical Disability - Family -- description: HMIS Data Element R13.15
        entry_screen.health_phys_disability_documented, -- label: Entry Screen Physical Documented -- description: Physical disabling condition is documented. HMIS Data Element 4.05.2B
        entry_screen.health_phys_disability_longterm, -- label: Entry Screen Physical Longterm -- description: Physical disabling condition expected to be long-continued and indefinite duration. HMIS Data Element 4.05.2A
        entry_screen.health_pregnancy, -- label: Entry Screen Pregnancy Status -- description: HMIS Data Element R10.1
        entry_screen.health_pregnancy_date, -- label: Entry Screen Pregnancy Status - Due Date -- description: HMIS Data Element R10.1A
        entry_screen.prior_duration, -- label: Entry Screen Prior Duration Raw -- description: HMIS Data Element 3.917AB.2
        entry_screen.income_private_disability, -- label: Entry Screen Private Disability Insurance -- description: HMIS Data Element 4.02.9G
        entry_screen.income_private_disability_is, -- label: Entry Screen Private Disability Insurance -- description: HMIS Data Element 4.02.9
        entry_screen.health_ins_ppay, -- label: Entry Screen Private Pay -- description: HMIS Data Element 4.04.9
        entry_screen.rhy_labor_exploitation_payment, -- label: Entry Screen Promised work, work or payment different -- description: HMIS Data Element R16.2
        entry_screen.rhsap_worst_housing, -- label: Entry Screen RHSAP Worst Housing -- description: HMIS Data Element U1
        entry_screen.path_not_enrolled_reason, -- label: Entry Screen Reason not Enrolled -- description: HMIS Data Element P3.2A
        entry_screen.hopwa_hiv_adap, -- label: Entry Screen Receiving AIDS Drug Assistance Program (ADAP) -- description: HMIS Data Element W3.3
        entry_screen.hopwa_hiv_services, -- label: Entry Screen Receiving Public HIV/AIDS Medical Assistance -- description: HMIS Data Element W3.2
        entry_screen.rhy_referral_src, -- label: Entry Screen Referral Source -- description: HMIS Data Element R1.1
        entry_screen.rhy_referral_freq_approached, -- label: Entry Screen Referral Source: Times Approached Prior to Project Start -- description: HMIS Data Element R1.1A
        entry_screen.ssvf_targeting_field_1, -- label: Entry Screen Referred by Coordinated Entry or a Homeless Assistance Provider to Prevent the Household From Entering an Emergency Shelter or Transitional Housing or From Staying in a Place Not Meant for Human Habitation. (Yes / No) -- description: HMIS Data Element V7.1
        entry_screen.ssvf_targeting_field_12, -- label: Entry Screen Registered Sex Offender (Yes / No) -- description: HMIS Data Element V7.12
        entry_screen.relationship_to_hoh, -- label: Entry Screen Relationship to Head of Household -- description: HMIS Data Element 3.15.1
        entry_screen.ssvf_targeting_field_7, -- label: Entry Screen Rental Evictions Within the Past 7 Years (Yes / No) -- description: HMIS Data Element V7.7
        entry_screen.prior_residence, -- label: Entry Screen Residence Prior to Project Entry -- description: HMIS Data Element 3.917AB.1
        entry_screen.income_ss_retirement, -- label: Entry Screen Retirement Income from Social Security -- description: HMIS Data Element 4.02.13K
        entry_screen.benefits_schip, -- label: Entry Screen SCHIP -- description: On State Children Health Insurance Program. HMIS Data Element 4.04.5
        entry_screen.soar_connected, -- label: Entry Screen SOAR Connected -- description: Client is connected to SSI/SSDI Outreach, Access and Recovery technical assistance program. HMIS Data Element P4.1
        entry_screen.income_ssdi, -- label: Entry Screen SSDI Amount -- description: HMIS Data Element 4.02.6D
        entry_screen.income_ssi, -- label: Entry Screen SSI Amount -- description: HMIS Data Element 4.02.5C
        entry_screen.rhy_school_status, -- label: Entry Screen School Status -- description: HMIS Data Element R5.1
        entry_screen.benefits_section8, -- label: Entry Screen Section 8 -- description: Retired HMIS Data Element
        entry_screen.rhy_sexual_orientation, -- label: Entry Screen Sexual Orientation -- description: HMIS Data Element R3.1
        entry_screen.rhy_sexual_orientation_other, -- label: Entry Screen Sexual Orientation - Other -- description: HMIS Data Element R3.1A
        entry_screen.ssvf_targeting_field_14, -- label: Entry Screen Single Parent With Minor Child(ren) (Yes / No) -- description: HMIS Data Element V7.14
        entry_screen.income_ssdi_is, -- label: Entry Screen Social Security Disability Insurance (SSDI) -- description: HMIS Data Element 4.02.6
        entry_screen.income_ss_retirement_is, -- label: Entry Screen Social Security Retirement Income -- description: HMIS Data Element 4.02.13
        entry_screen.benefits_wic, -- label: Entry Screen Special Supplemental Nutrition Program for Women, Infants, and Children (WIC) -- description: Receiving Women, Infants and Children benefits. HMIS Data Element 4.03.4
        entry_screen.prior_state, -- label: Entry Screen State -- description: HMIS Data Element V5.3
        entry_screen.health_ins_state, -- label: Entry Screen State Insurance for Adults -- description: HMIS Data Element 4.04.10
        entry_screen.path_status_determination, -- label: Entry Screen Status Determination Date -- description: HMIS Data Element P3.1
        entry_screen.prior_street_address, -- label: Entry Screen Street Address -- description: HMIS Data Element V5.1
        entry_screen.health_substance_abuse, -- label: Entry Screen Substance Abuse -- description: The client has a substance abuse disabling condition. HMIS Data Element 4.10.2
        entry_screen.health_substance_abuse_confirmed, -- label: Entry Screen Substance Abuse Confirmed (PATH) -- description: Retired HMIS Data Element. PATH Only: How was substance abuse disabling condition confirmed.
        entry_screen.health_substance_abuse_documented, -- label: Entry Screen Substance Abuse Documented -- description: Retired HMIS Data Element. Substance abuse disabling condition is documented.
        entry_screen.health_substance_abuse_longterm, -- label: Entry Screen Substance Abuse Longterm -- description: Substance abuse disabling condition expected to be long-continued and indefinite duration. HMIS Data Element 4.10.2A
        entry_screen.ssvf_targeting_field_5, -- label: Entry Screen Sudden and Significant Decrease in Cash Income (Employment and/or Cash Benefits) AND/OR Unavoidable Increase in Non-Discretionary Expenses (e.g., Rent or Medical Expenses) in the Past 6 Months (Yes / No) -- description: HMIS Data Element V7.5
        -- entry_screen.benefits_snap, -- label: Entry Screen Supplemental Nutrition Assistance Program (SNAP) (Previously known as Food Stamps) -- description: Supplemental Nutrition Assistance Program (SNAP; Previously known as Food Stamps). HMIS Data Element 4.03.3
        entry_screen.income_ssi_is, -- label: Entry Screen Supplemental Security Income (SSI) -- description: HMIS Data Element 4.02.5
        entry_screen.tcell_count_available, -- label: Entry Screen T-cell (CD4) Count Available -- description: HMIS Data Element W4.2
        entry_screen.tcell_count_number, -- label: Entry Screen T-cell Count -- description: (integer between 0 - 1500) HMIS Data Element W4.2A
        entry_screen.tcell_data_obtained, -- label: Entry Screen T-cell Count obtained -- description: How Was the information Obtained (T-cell Count) HMIS Data Element W4.2B
        entry_screen.income_tanf_is, -- label: Entry Screen TANF -- description: HMIS Data Element 4.02.11I
        entry_screen.benefits_tanf_childcare, -- label: Entry Screen TANF Child Care services -- description: Receiving TANF Childcare benefits. HMIS Data Element 4.03.5
        entry_screen.benefits_tanf_transportation, -- label: Entry Screen TANF transportation services -- description: Receiving TANF transportation services benefits. HMIS Data Element 4.03.6
        entry_screen.benefits_temp_rent, -- label: Entry Screen Temporary Rental Assistance -- description: Retired HMIS Data Element
        entry_screen.th_ph_less_than_7_nights, -- label: Entry Screen Th Ph Less Than 7 Nights Raw -- description: For transitional and permanent housing situations, was stay less than 7 days HMIS Data Element 3.917B.2B
        entry_screen.chronic_2, -- label: Entry Screen Times Homeless in the Past Three Years -- description: Times homeless, including this time, in past three years. HMIS Data Element 3.917AB.4
        entry_screen.income_individual, -- label: Entry Screen Total Cash Income -- description: Sum of cash income from all sources for individual. HMIS Data Element 4.02.18
        entry_screen.chronic_3, -- label: Entry Screen Total Months Homeless in Past Three Years -- description: Total months homeless in past three years. HMIS Data Element 3.917AB.5
        entry_screen.rhy_crit_unemployment_family, -- label: Entry Screen Unemployment - Family -- description: HMIS Data Element R13.9
        entry_screen.income_unemployment, -- label: Entry Screen Unemployment Amount -- description: HMIS Data Element 4.02.4B
        entry_screen.income_unemployment_is, -- label: Entry Screen Unemployment Income -- description: HMIS Data Element 4.02.4
        entry_screen.ref_user, -- label: Entry Screen User Creating -- description: User that created Project Update, Annual Assessment, Exit or Post Exit. HMIS Data Element 5.07
        entry_screen.ref_user_updated, -- label: Entry Screen User Updating -- description: User that updated Project Update, Annual Assessment, Exit or Post Exit. HMIS Data Element 5.07
        entry_screen.benefits_va_medical, -- label: Entry Screen VA Medical Insurance -- description: On VA Medical Insurance. HMIS Data Element 4.04.6
        entry_screen.vamc_station_number, -- label: Entry Screen VAMC Station Number -- description: HMIS Data Element V6.1: VAMC Station Number
        entry_screen.income_vet_disability_is, -- label: Entry Screen Veteran Disability -- description: HMIS Data Element 4.02.7
        entry_screen.income_vet_pension_is, -- label: Entry Screen Veteran Pension -- description: HMIS Data Element 4.02.8F
        entry_screen.viral_load_available, -- label: Entry Screen Viral Load Information Available -- description: HMIS Data Element W4.3
        entry_screen.viral_load_number, -- label: Entry Screen Viral Load Number -- description: Count (integer between 0 - 999999) HMIS Data Element W4.3C
        entry_screen.viral_load_obtained, -- label: Entry Screen Viral Load Obtained -- description: How Was the information Obtained (Viral Load). HMIS Data Element W4.3D
        entry_screen.income_workers_comp_is, -- label: Entry Screen Workers Comp -- description: HMIS Data Element 4.02.10
        entry_screen.zipcode -- label: Entry Screen ZIP Code -- description: HMIS Data Element V5.4
    
  FROM client_programs AS enrollments
  JOIN client_program_demographics AS entry_screen ON enrollments.id = entry_screen.ref_program
 WHERE entry_screen.screen_type = 2
 LIMIT 10;