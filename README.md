
## Purpose
The purpose of this document and repository is to make the most out of the Clarity Customer SQL Model. This repository will focus primarily on the HUD data elements, which are Enrollment centric. However, pulling custom data fields will be covered as well. The examples provided are not exhaustive, rather, they are starter queries meant to assist query development.

Important documents:
* [HUD HMIS Data Dictionary](https://files.hudexchange.info/resources/documents/HMIS-Data-Dictionary.pdf)
* [Clarity SQL Model Documentation](https://help.bitfocus.com/customer-sql-data-model)


## Profile
In Clarity, personal identifying information is often referred to as "Profile" data, as most of these fields can be found on the Clarity "Profile" screen. However, in the Data Dictionary these fields are part of the "Universal Data Element." To further confuse things, these fields are stored in several views in Clarity Customer SQL Model:

| Clarity App Name | Clarity DB View Name             | HUD Common Name                                            |
|------------------|----------------------------------|------------------------------------------------------------|
| Profile          |    clarity_clients               | Universal Data Elements / Personal Identifying Information |
| Profile          |    clarity_client_demographics   | Universal Data Elements / Personal Identifying Information |

### Join Example
The join keys are `clarity_clients.id = clarity_client_demographics.ref_client`

For example:
```sql
  FROM clarity_clients AS clients
  JOIN clarity_client_demographics AS static_demographics ON clients.id = static_demographics.ref_client
```

### Sample Queries
Sample query:
* [Client Demographics](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/client_demographics.sql). E.g., `Race`, `DOB`, `SSN`, etc.


## Enrollments and HUD Assessments
Enrollment information are contained in two views, `client_programs` and `client_program_demographics`. These will contain all of the Clarity Enrollment data and the corresponding HUD Assessments for Entry, Exit, Annual, Update, and Followup assessments. These tables correspond to HUD elements found in the `Universal Data Elements`, `Program Specific`, and `Federal Partner` sections of the HMIS Data Dictionary.


| Clarity App Name                       | Clarity DB View Name                     | HUD Common Name                                              |
|----------------------------------------|------------------------------------------|--------------------------------------------------------------|
| Enrollments                            |    clarity_client_programs               | Universal Data Elements / Program Specific / Federal Partner |
| Entry / Update / Exit Screen           |    clarity_client_program_demographics   | Universal Data Elements / Program Specific / Federal Partner |

### Join Example
The join keys are `clarity_client_programs.id = clarity_client_program_demographics.ref_program`

For example:
```sql
  FROM client_programs AS enrollments
  JOIN client_program_demographics AS hud_assessments ON enrollments.id = hud_assessments.ref_program AND hud_assessments.deleted IS NULL
```

It is important to understand the `client_programs` table is largely normalized. That is, it contains one entry per client Enrollment.  However, the `client_program_demographics` table contains entries for all collection stages.  The data collection stage is identified by the field `client_program_demographics.screen_type`.

| Clarity App Name                       | Clarity DB Field Name                                         | HUD Common Name                 |
|----------------------------------------|--------------------------------------------------------------|----------------------------------|
| Entry Screen                           |    client_program_demographics.screen_type = 2               |  Project Start                   |
| Update Screen                          |    client_program_demographics.screen_type = 3               |  Project Update                  |
| Update Screen                          |    client_program_demographics.screen_type = 6               |  Project Annual Assessment       |
| Exit Screen                            |    client_program_demographics.screen_type = 4               |  Project Exit                    |
| Followup Screen                        |    client_program_demographics.screen_type = 5               |  Post Exit                       |

Often it will be helpful for the query to denormalize data collection stages. I.e., have a corresponding Entry and Exit data in the same row.  This can be accomplished with a join like:
```sql
    SELECT enrollments.id                                      AS enrollment_id,
           enrollments.start_date                              AS start_date,
           enrollments.end_date                                AS end_date,
           entry_screen.prior_residence                        AS prior_residence,
           last_screen.exit_destination                        AS destination
      FROM clarity_client_programs AS enrollments
      JOIN clarity_client_program_demographics AS entry_screen 
        ON enrollments.id = entry_screen.ref_program 
       AND entry_screen.screen_type = 2 -- Entry Screen
       AND entry_screen.deleted IS NULL
 LEFT JOIN clarity_client_program_demographics AS last_screen 
        ON enrollments.id = last_screen.ref_program 
       AND last_screen.screen_type = 4 -- Exit Screen
       AND last_screen.deleted IS NULL
     WHERE enrollments.deleted IS NULL
     LIMIT 10;
```


### Sample Queries

Sample queries:
* [Enrollments by Data Collection Stage](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/enrollments_by_data_collection_stage.sql)
* [HMIS Elements on Entry Screen](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/entry_screen.sql)
* [HMIS Elements on Exit Screen](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/last_screen.sql)
* [Entry and Exit Fields](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/entry_exit_screens.sql)

## Assessments
In a continuum-of-care (CoC) [HUD prefers every](https://www.hud.gov/sites/documents/16-11CPDN.PDF) client to have an "Coordinated Entry Assessment" (CEA) to determine the level of assistance an individual may need to exit homelessness. This is often referred to simply as an "Assessment," however, it is important to distinguish a CEA from a HUD Assessment, or a locally required assessment. The most common assessment tool used for assessing an individual's need is the VI-SPDAT, but not every community chooses to use the VI-SPDAT. The Clarity data schema accommodates a CoC's choice with a flexible assessment structure.

| Clarity App Name                       | Clarity DB View Name                                         | HUD Common Name                  |
|----------------------------------------|--------------------------------------------------------------|----------------------------------|
| Assessment                             | clarity_screen                                               | Coordinated Entry Assessment     |
| Assessment                             | clarity_client_assessments                                   | Coordinated Entry Assessment     |
| Assessment                             | clarity_client_assessment_demographics                       | Coordinated Entry Assessment     |
| Assessment                             | clarity_client_assessment_scores                             | Coordinated Entry Assessment     |
| Assessment                             | clarity_program_assessments                                  | Coordinated Entry Assessment     |


### Join Example

For all assessments:
```sql
...
     FROM clarity_clients AS client
LEFT JOIN clarity_client_assessment_demographics AS client_assessments ON client.id = client_assessments.ref_client AND client_assessments.deleted is NULL 
LEFT JOIN clarity_client_assessment_scores  AS client_assessment_scores ON client_assessments.id = client_assessment_scores.ref_assessment
     JOIN clarity_screens AS screens ON screens.id = client_assessments.ref_assessment
    WHERE client.deleted IS NULL    
```

As mentioned, all assessment data are stored in the same structure, however, the field `screens.coordinated_entry` will be set to `TRUE` if the assessment is part of the official Coordinated Entry Assessment in the CoC. This allows the query writer to quickly filter to official HUD Assessments.

For example,
```sql
    ...
LEFT JOIN clarity_client_assessment_scores  AS client_assessment_scores ON client_assessments.id = client_assessment_scores.ref_assessment
     JOIN clarity_screens AS screens ON screens.id = client_assessments.ref_assessment
    WHERE client.deleted IS NULL 
      AND screens.coordinated_entry IS TRUE 
```

### Sample Queries

Sample queries:
* [Assessment Score by Client with Assessing Agency Name](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/client_assessments_with_scores.sql)
* [Most Recent Coordinated Entry Assessment by Client](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/most_recent_coordinated_entry_assessment_by_client.sql)

## Program and Agency Setup
Next to Profile and Enrollment data, the Program and Agency data are the most important. They represent the provider portion in the client-provider relationship. In Clarity, there are many agencies and each of these agencies may have many programs. 

| Clarity App Name                       | Clarity DB View Name                                         | HUD Common Name                  |
|----------------------------------------|--------------------------------------------------------------|----------------------------------|
| Programs                               | clarity_programs                                             | Project                          |
| Programs                               | clarity_programs_coc                                         | CoC                              |
| Agencies                               | clarity_agencies                                             | Organization                     |
| Inventory                              | clarity_program_inventory                                    | Bed and Unit Inventory           |


### Join Example
To get Program and Agencies information:
```sql
....
    FROM clarity_agencies AS agencies
    JOIN clarity_programs AS programs ON agencies.id = programs.ref_program
```

To get Bed and Unit inventory information:
```sql
....
   FROM clarity_programs AS programs
   JOIN clarity_program_inventory AS program_inventory ON programs.id = program_inventory.ref_program
```
### Sample Queries

Sample queries:
* [Project Descriptor Fields](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/project_descriptor_fields.sql)
* [Bed and Unit Inventory Fields](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/bed_and_unit_inventory.sql)

## Custom Clarity Fields
The Clarity application is extraordinarily versatile. One of the options which empowers local reporting metrics is the Clarity custom field option.

* [Creating a Clarity Custom Field](https://get.clarityhs.help/hc/en-us/articles/115000497408-Creating-and-Editing-Fields)

What is really neat is these fields are then available to report on. Though, it can seem a little tricky at first. The steps below will explain how fields can be retrieved through the customer SQL model.

First, it is important to understand how these fields are stored. The custom Clarity fields are stored in [dynamic columns](https://mariadb.com/kb/en/dynamic-columns/) inside `blob` fields on the field's respective table.

Here is a list of the possible storage fields for Clarity custom fields:
| View Name                           |  Column Name         | Clarity Name            |
|-------------------------------------|----------------------|-------------------------|
| clarity_client_assessment_data      |  custom_data         | Custom Assessments      | 
| clarity_client_data                 |  custom_data         | Custom Profile          |
| clarity_client_program_data         |  custom_data         | Custom Enrollments      |
| clarity_encampments                 |  custom_data         | Custom Outreach         |
| clarity_encampments_history         |  custom_data         | Custom Outreach         |
| clarity_funding_source_data         |  custom_data         | Custom Funding Sources  |
| clarity_geolocations                |  custom_data         | Custom Locations        |
| clarity_program_data                |  custom_data         | Custom Programs         |
| clarity_program_openings            |  custom_data         | Custom Program Openings |

Before we can retrieve the contents of a custom field, we need to know the field's database name. This can be found in Clarity by navigating to the `Field Editor`

![clarity-field-editor](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/raw/master/images/custom-fields-1.png)

Then search for the field by its "display name" (how it appears in Clarity). Once you've found the field, copy the name. This should be similar to the display name, but all lower-case and underscores instead of spaces.

![clarity-custom-field-search](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/raw/master/images/custom-fields-2.png)

Now we have the field name, let's use the MariaDB built in function called [COLUMN_GET()](https://mariadb.com/kb/en/dynamic-columns/#column_get) to retrieve the field.

For example:
```sql
SELECT assessments.id                                                                                       AS assessment_id,
       assessments.ref_client                                                                               AS personal_id,
       COLUMN_GET(custom_assessments.custom_data,'c_did_you_provide_leadership_programming' AS CHAR(255))   AS provide_leadership
  FROM client_assessment_demographics AS assessments
  JOIN client_assessment_data AS custom_assessments ON assessments.id = custom_assessments.ref_client_assessment_demographics
 WHERE COLUMN_GET(custom_assessments.custom_data,'c_did_you_provide_leadership_programming' AS CHAR(255)) IS NOT NULL;
```

The astute query writer may notice the values being returned are codified. That is, they are the picklist number item corresponding with the text item. This is helpful when writing exact filters, however, not great when trying to provide human readable information. However, just below picklist text value retrieval will be covered.

## Sample Queries
* None, as will be specific to instance.

## Use of Picklist Function
A lot of values in the Clarity DB are stored as key-value pairs. This is known as a "picklist."

For example:
| # field_name         |  code |  value_name                                                | 
|----------------------|-------|------------------------------------------------------------| 
|  program_categories  |   14  |   Coordinated Entry                                        | 
|  program_categories  |   11  |   Day Shelter                                              | 
|  program_categories  |   1   |   Emergency Shelter                                        | 
|  program_categories  |   12  |   Homeless Prevention                                      | 
|  program_categories  |   7   |   Other                                                    | 
|  program_categories  |   9   |   PH - Housing Only                                        | 
|  program_categories  |   10  |   PH - Housing with Services (no disability required)      | 
|  program_categories  |   3   |   PH - Permanent Supportive Housing (disability required)  | 
|  program_categories  |   13  |   PH - Rapid Re-Housing                                    | 
|  program_categories  |   5   |   RETIRED (HPRP)                                           | 
|  program_categories  |   8   |   Safe Haven                                               | 
|  program_categories  |   6   |   Services Only                                            | 
|  program_categories  |   4   |   Street Outreach                                          | 
|  program_categories  |   2   |   Transitional Housing                                     | 

The `code` field is great for precision reporting, but not helpful when trying to create human readable reports. The Clarity Customer SQL Model has a built in function for translating between the `code` value and the `value_name` (text value): `fn_c_getPicklistValueName`.

The `fn_c_getPicklistValueName()` takes two arguments.
* Field name as a string. E.g., `program_categories`, `c_did_you_provide_leadership_programming`, etc.
* Field code. E.g, `1`, `2`, `3`.

Calling the function may look something like this:

```sql
 SELECT
        ...
        fn_c_getPicklistValueName('program_categories', p.ref_category) 		AS project_type,
    JOIN clarity_programs AS p ON cp.ref_program = p.id
...
```

The `fn_c_getPicklistValueName` can also be used in combination with the `COLUMN_GET` function to return the text value of a custom Clarity field.

For example:
```sql
 SELECT assessments.id                                                                                         AS assessment_id,
        assessments.ref_client                                                                                 AS personal_id,
        CASE WHEN COLUMN_GET(custom_assessments.custom_data,'c_did_you_provide_leadership_programming' AS CHAR(255)) != ""
             THEN fn_getPicklistValueName('c_did_you_provide_leadership_programming', 
                    COLUMN_GET(custom_assessments.custom_data, 'c_did_you_provide_leadership_programming' AS INT)
                  )   
             ELSE NULL
       END AS provide_leadership
  FROM client_assessment_demographics AS assessments
  JOIN client_assessment_data AS custom_assessments ON assessments.id = custom_assessments.ref_client_assessment_demographics
 WHERE COLUMN_GET(custom_assessments.custom_data,'c_did_you_provide_leadership_programming' AS CHAR(255)) != "";
...
```
Note the `COLUMN_GET(custom_assessments.custom_data,'c_did_you_provide_leadership_programming' AS CHAR(255)) != ""` call as some values are not numeric, this ensures only numeric results are passed to the `fn_getPicklistValueName()` 

## Useful Query Techniques

### First / Last Filtering
Often the query writer will need to get the first or last of records per client. Let's say, for example, your CoC only uses the most recent Coordinated Entry Assessment, let's look at a query to pull back the most recent CE assessment per client.

```sql
-- Create a filter CTE
WITH most_recent_assessments (personal_id, assessment_date) AS (
    SELECT cad.ref_client                     AS personal_id,
           MAX(cad.assessment_date)           AS assessment_date
      FROM clarity_client_assessment_demographics AS cad
      JOIN clarity_screens AS screens ON screens.id = cad.ref_assessment
  	 WHERE screens.coordinated_entry IS TRUE
  GROUP BY cad.ref_client
)
-- Primary query
   SELECT
          client_assessments.ref_client                                                      AS clients_id,
          client_assessments.id                                                              AS assessment_id,
          screens.name                                                                       AS assessment_name,
          client_assessment_scores.score                                                     AS coordinated_entry_assessment_score
     FROM clarity_clients AS client
LEFT JOIN clarity_client_assessment_demographics AS client_assessments ON client.id = client_assessments.ref_client AND client_assessments.deleted is NULL 
LEFT JOIN clarity_client_assessment_scores  AS client_assessment_scores ON client_assessments.id = client_assessment_scores.ref_assessment
     JOIN clarity_screens AS screens ON screens.id = client_assessments.ref_assessment
     -- The INNER JOIN should filter out all assessments but the most recent per client.
     JOIN most_recent_assessments 
       ON client_assessments.ref_client = most_recent_assessments.personal_id
      AND client_assessments.assessment_date = most_recent_assessments.assessment_date
    WHERE client.deleted IS NULL    
      AND screens.coordinated_entry IS TRUE;
```

This query uses a [Common Table Expression](https://mariadb.com/kb/en/with/) (CTE) to pull back the most recent entry CE assessment date per client. The CTE's results are then inner joined on the `assessment_date` in the main query. This acts as a filter, reducing the results set in the main query to only the most recent per client. 

This exact same query can be used to retrieve the first CE assessment per client with only one modification. Changing the `MAX` to `MIN` in the CTE results in the first CE assessment per client.
```sql
WITH earliest_assessments (personal_id, assessment_date) AS (
    SELECT cad.ref_client                     AS personal_id,
           MIN(cad.assessment_date)           AS assessment_date
      FROM clarity_client_assessment_demographics AS cad
...
```
#### Sample Queries

Sample queries:
* [Most Recent Coordinated Entry Assessment per Client](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/most_recent_coordinated_entry_assessment_by_client.sql)
* [First HUD Assessment per Client]()
  
### Active
A common request is to determine what clients are still active in a program. Or who is enrolled in a shelter. This can sometimes be tricky for a query writer, as client's activity is represented by `start_date` and `end_date` and the "report range" is also represented by two dates, `report_start_date` and `report_end_date`. The query writer will be asked to determine which clients are active during the report range. Let's look at an example.

Let's say we have the following enrollments in our database.
| enrollment_id      | start_date          | end_date            |
|--------------------|---------------------|---------------------|
| 352                | 2021-01-05          | 2021-03-04          |
| 1253               | 2021-01-04          | NULL                |
| 9075               | 2021-04-10          | 2021-04-15          |

And the query writer has been asked to determine which clients had an active enrollment between `2021-01-01` and `2021-03-08`. These dates would become the `report_start_date` and `report_end_date` respectively. This would result be Enrollments `352` and `1253` are active.

| enrollment_id      | start_date          | end_date            | Active    |
|--------------------|---------------------|---------------------|-----------|
| 352                | 2021-01-05          | 2021-03-04          | Yes       |
| 1253               | 2021-01-04          | NULL                | Yes       |
| 9075               | 2021-04-10          | 2021-04-15          | No        |

We can use the following formula to accomplish this (pseudocode):
```bash
  [Report Start Date] <= COALESCE([Project End Date], NOW())
  [Report End Date]   >= [Project Start Date]
```
If you would like to know more about why this formula finds active enrollments, I'd recommend this excellent explanation:
* [Answer to "Determine Whether Two Date Ranges Overlap"](https://stackoverflow.com/a/325964/2108441)

We can apply it in our SQL like this:
```sql
SELECT enrollments.id                  AS enrollment_id,
       enrollments.start_date          AS project_start_date,
       enrollments.end_date            AS project_end_date
  FROM client_programs AS enrollments
 WHERE DATE("2021-01-01") <= COALESCE(enrollments.end_date, NOW())
   AND DATE("2021-03-08") >= enrollments.end_date
```
Note, the `enrollments.end_date` is [coalesced](https://mariadb.com/kb/en/coalesce/) with today's date, as active enrollments have `NULL` for end dates, and comparing a date to `NULL` always fails.

#### Sample Queries

Sample queries:
* [Get Active Enrollments](https://gitlab.com/bitfocus-public/clarity_custom_sql_model/-/blob/master/queries/active_enrollments.sql)
