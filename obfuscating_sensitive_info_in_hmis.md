## Obfuscating HMIS Data in MySQL / MariaDB Queries
When working with HMIS data one is often using SQL to query sensitive data to provide it to a third party. Almost as often, these requesters should not have access to any sensitive data from the HMIS. Their access to the data is meant to facilitate an audit. Or used for research. In these cases, the data are important, but not necessarily the personal identifying information contained therein. Below is a process by which an analyst can obfuscate the sensitive data while leaving the rest when querying a MariaDB or MySQL database.

```sql
SELECT  
   HEX( AES_ENCRYPT(c.id, SHA2('sensitive_key', 512) ) )        AS encrypted_personal_id,
   AES_DECRYPT(
 	 UNHEX(
          HEX( AES_ENCRYPT(c.id, SHA2('sensitive_key', 512) ) )
    ),
   	 SHA2('sensitive_key', 512)
   )                                                            AS decrypted_personal_id,
   c.id                                                         AS personal_id
FROM clients AS c
```
This query returns three fields:
* `encrypted_personal_id`
* `decrypted_personal_id`
* `personal_id`

![query results](./images/query_results.png)

The `encrypted_personal_id` is the result of passing the `clients.id` value from the database into the `AES_ENCRYPT()` function built into the MySQL / MariaDB server. Of course, the function needs some additional secret data, whereby the `clients.id` can be decrypted.  Here, we provide a unique ID specific to the requester. This ID will need to be treated as a password and kept in a secure system, such as a password manager. But we don't simply pass the `sensitive_key` into the encryption function. First, we hash it using [SHA2](https://en.wikipedia.org/wiki/SHA-2). This ensures the key is scrambled enough statistical processes can not reasonably extract it from the data [[1](https://security.stackexchange.com/a/179860)]. The result of hashing the `sensitive_key` is then passed into the `AES_ENCRYPT()`. The result is a binary string. Unfortunately, a binary string is not easy for humans to work with, so the `HEX()` converts the binary string into base `16`. Or, characters between `0` and `F`. The result is a hexadecimal string, which should be friendly to work with in Excel or simply on a hardcopy of the data.

The SQL producing the `decrypted_personal_id` shows how to reverse the process. 

This process provides several advantages:
* Each requester will have an ID unique to them encoded into the requested data. Much like a finger print, if these data appear in the public, then the governing agency could take all the `sensitive_keys`, for all requesters, and pass `encrypted_personal_id` through the decrypt process. The `sensitive_key` which produces valid data would be the original requester.
* Every requester's data IDs will be unique to them. This ensures no two data requesters are able to correlate their data.

Of course, disadvantages exist too:
* Discovering the ID of a requester requires access to all `sensitive_keys`
* Each sensitive ID encrypted through this process must be wrapped with the encryption SQL.

One word of warning, this example is simply a thought exercise. Before implementing any such process, an organization should consult their information security officer. Use at your own risk.